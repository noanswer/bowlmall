package com.mall.api.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.validator.constraints.Length;

import lombok.Data;
import lombok.Getter;

@Data
@Getter
@Entity
@Table(name = "a_product")
public class ProductDTO {

  @Id
  @Column(name = "product_id", unique = true)
  private String product_id;

  @Column(name = "product_name")
  @Length(max = 50, message = "Invalid product_name length")
  private String product_name;

  @Column(name = "product_cnt")
  private Integer product_cnt;
  
  @Column(name = "product_type")
  @Length(max = 2, message = "invalid product_type length")
  private String product_type;
  
  @Column(name = "product_size")
  private Integer product_size;

}
