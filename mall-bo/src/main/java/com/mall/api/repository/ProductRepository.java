package com.mall.api.repository;

import org.springframework.data.repository.CrudRepository;

import com.mall.api.entity.ProductDTO;

public interface ProductRepository extends CrudRepository<ProductDTO, Long> {

}
