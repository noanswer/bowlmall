package com.mall.api.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.mall.api.entity.ProductDTO;
import com.mall.api.repository.ProductRepository;

import lombok.extern.slf4j.Slf4j;

@RestController
@RequestMapping(value = "/product")
@Slf4j
public class ProductController {
	
	@RequestMapping(value = "/items")
	public List<ProductDTO> getItems()
	{
		List<ProductDTO> result = (List<ProductDTO>) productRepository.findAll();
		
//		for (ProductDTO product:)
//		{
//			result.add(product)
//			log.info(product.toString());
//		}
		
		return result; 
	}
	
	@Autowired
	ProductRepository productRepository;
}
