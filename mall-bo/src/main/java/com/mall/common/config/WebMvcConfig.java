package com.mall.common.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
public class WebMvcConfig implements WebMvcConfigurer {

  @Bean
  public BCryptPasswordEncoder passwordEncoder() {
    return new BCryptPasswordEncoder();
  }

  /**
   * The example above enables CORS requests from any origin to any endpoint in the application.
   * 
   * If we want to lock this down a bit more, the registry.addMapping method returns a
   * CorsRegistration object which can be used for additional configuration. There’s also an
   * allowedOrigins method which you can use to specify an array of allowed origins. This can be
   * useful if you need to load this array from an external source at runtime.
   * 
   * Additionally, there’s also allowedMethods, allowedHeaders, exposedHeaders, maxAge, and
   * allowCredentials which can be used to set the response headers and provide us with more
   * customization options.
   */
  @Override
  public void addCorsMappings(CorsRegistry registry) {
    registry.addMapping("/**").allowedOrigins("http://localhost:3000").allowedMethods("POST",
        "OPTIONS");
  }

}
