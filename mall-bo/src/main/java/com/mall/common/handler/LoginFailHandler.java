package com.mall.common.handler;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.AuthenticationFailureHandler;

public class LoginFailHandler implements AuthenticationFailureHandler {

  @Override
  public void onAuthenticationFailure(HttpServletRequest request, HttpServletResponse response,
      AuthenticationException exception) throws IOException, ServletException {
    response.setContentType("application/json");

    JSONObject jsonObject = new JSONObject();

    try {
      jsonObject.put("result", "fail");
    } catch (JSONException e) {
      e.printStackTrace();
    }

    PrintWriter writer = response.getWriter();
    writer.println(jsonObject);
    writer.flush();
  }

}
