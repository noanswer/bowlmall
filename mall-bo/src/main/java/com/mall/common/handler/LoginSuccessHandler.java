package com.mall.common.handler;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;

public class LoginSuccessHandler implements AuthenticationSuccessHandler {

  @Override
  public void onAuthenticationSuccess(HttpServletRequest request, HttpServletResponse response,
      Authentication authentication) throws IOException, ServletException {
    response.setContentType("application/json");

    JSONObject jsonObject = new JSONObject();

    try {
      jsonObject.put("result", "success");
    } catch (JSONException e) {
      e.printStackTrace();
    }

    PrintWriter writer = response.getWriter();
    writer.println(jsonObject);
    writer.flush();
  }

}
